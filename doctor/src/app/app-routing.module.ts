import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  // {
  //   path: 'home/:id',
  //   loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  // },
  // {
  //   path: 'history/:id',
  //   loadChildren: () => import('./pages/history/history.module').then( m => m.HistoryPageModule)
  // },
  { 
    //id1=uid
    path: 'firstloadpage/:uid',
    children:[
      {
        path:'',
        loadChildren: () => import('./pages/firstloadpage/firstloadpage.module').then( m => m.FirstloadpagePageModule)
      },
      // id=did
      {
        path : 'history/:did',
        children : [
          {
            path: '',
            loadChildren: () => import('./pages/history/history.module').then( m => m.HistoryPageModule)
          },
        ] 
      },
      // id=did
      {
        path: 'appointments/:did',
        children:[
          {
            path: '',
            loadChildren: () => import('./pages/appointments/appointments.module').then( m => m.AppointmentsPageModule)
          },
          {
            path: 'open-single-appointments/:bid',
            loadChildren: () => import('./pages/appointments/open-single-appointments/open-single-appointments.module').then( m => m.OpenSingleAppointmentsPageModule)
          },

        ]
      }

    ]
  },



  // {
  //   path: 'appointments/:id',
  //   loadChildren: () => import('./appointments/appointments.module').then( m => m.AppointmentsPageModule)
  // }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
