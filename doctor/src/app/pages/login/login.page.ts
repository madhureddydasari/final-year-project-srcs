import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { async } from '@angular/core/testing';
import { NavController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  username :string=""
  password:string=""

  constructor(
    private Aauth : AngularFireAuth,
    private navCtrl : NavController,
    public alert : AlertController,
    public router :Router
    ) { }

  ngOnInit() {
    this.Aauth.auth.onAuthStateChanged(user => {
      if(user)
      {
        this.navCtrl.navigateRoot(`/firstloadpage/${user.uid}/home/${user.uid}`)
      }
    })
  }

  async loginaction()
  {
    const { username,password } = this
    try{
      const res= await this.Aauth.auth.signInWithEmailAndPassword(username+'@doctormail.com',password)
      .then((user)=>{
        let uid=user.user.uid
        this.navCtrl.navigateRoot(`/firstloadpage/${uid}/home/${uid}`);
      })

    }
    catch(err){
      // if(err.code == "auth/invalid-email")
      // if(err.code == "auth/invalid-email")
      // {
        console.log("username and password doesnot match")
        this.showAlert("Error!","Invalid Username and Password")
      // }
      
      // else{
      //   console.log(err.message)
      // this.router.navigate(['/index/login'])
      // this.showAlert("Error!","Invalid Username and Password")
      // }
    }

  }


  async showAlert(header :string , message :string)
  {
    const alerrt = await this.alert.create({
      header,
      message,
      buttons: ["OK"]
    })
    await alerrt.present()
  }

}
