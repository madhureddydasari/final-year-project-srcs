import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController, AlertController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { bookingstruct } from 'src/app/modal/booking';
import { BookingService } from 'src/app/services/booking.service';
import { Doctorstruct } from 'src/app/modal/doctor';
import { DoctorService } from 'src/app/services/doctor.service';
import { StrToDateConversionService } from 'src/app/services/str-to-date-conversion.service';
import { AutoSlotUpdateService } from 'src/app/services/auto-slot-update.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.page.html',
  styleUrls: ['./history.page.scss'],
})
export class HistoryPage implements OnInit {

  toggler:boolean=false
  // todayDate:Date;
  // slotDate:Date;
  todaysDate : string = new Date().toLocaleDateString()
  bookingsdb1: Observable<bookingstruct[]>
  // doctorsdb : Observable<Doctorstruct>
  doctor : Doctorstruct ={
    name:'',
    description:'',
    department:'',
    phone: '',
    specialization: '',
    consultationFee : '',
    timings:'',
    facilities:'',
    createdAt: '',
    // docId : '',
  }
  // doctorsdb : Observable<Doctorstruct>

  constructor(private fbAutoSlotUpdateService : AutoSlotUpdateService,private fbTimeService : StrToDateConversionService,private fbDoctorService : DoctorService,private alertCtrl: AlertController, private fbBookingService: BookingService, private activatedRoute: ActivatedRoute, private navCtrl: NavController) 
  {  }

  async ngOnInit() {
    let id = this.activatedRoute.snapshot.paramMap.get('did')
    if(id) {
      this.bookingsdb1 = this.fbBookingService.getBookingsInParticular(id)
      this.fbDoctorService.getDoctor(id).subscribe( (docData) => {
        this.doctor =  docData;
        // console.log(docData);
        // console.log("inside inner" + this.doctor);
        this.fbAutoSlotUpdateService.updation(this.bookingsdb1,this.doctor);
      })
    // console.log("inside outer"+this.doctor);

    }
  }

  // this function is made into a service ,So that number of pages can use this unctioin agaian and again 
  // this is called code reusability
  updateAllStatus()
  {
    // this.bookingsdb1.subscribe((docdata) =>{ 
    //   // console.log(docdata);
    //   for(let i=0 ; i<docdata.length ; i++)
    //   {
    //     if(docdata[i].status == 'unvisited')
    //     {
    //       if(this.convertToDate(docdata[i].slotDate) < this.convertToDate(this.todaysDate))
    //       {
    //         this.fbBookingService.autoUpdateBookingStatus(docdata[i].id)
    //       }
    //     }
    //   }
    //  }) ;
  }

  convertToDate(x: string)
  {
    return this.fbTimeService.localeDateStringToDate(x)
  }



  goBack() {
    this.navCtrl.back();
  }






  // async updateBookingStatus(booking : bookingstruct)  {
  //   // console.log(new Date('04/01/2011'))
  //   // console.log(new Date('2011/04/01'))

  //   // console.log(new Date('04/02/2020'))

  //   // console.log(new Date('2011/04/01'))
  //   // console.log(new Date('2020/04/02'))

  //   // let y= this.fbTimeService.localeDateStringToConvertibleDateString('01/04/2011')
  //   // console.log(new Date(y));
    
    

  //   await this.alertCtrl.create({
  //     message: "are you sure to update status to VISITED or expired ",
  //     buttons: [
  //       { text: "cancel",
  //         handler: () => {
  //         }
  //       },
  //       {
  //         text: "ok",
  //         handler: async () => {
  //           this.doctor.pendingBookings = this.doctor.pendingBookings - 1  // first update pending bpokings and then ther itself in doctor service
  //                       // call the booking service which is just after this line , it is as done in hms user  app
  //         // await this.fbBookingService.updateBookingStatus(booking)
  //           await this.fbDoctorService.updateDoctorPendingBookings(this.doctor,booking)

  //         }
  //       }]
  //   }).then((alert) => {
  //     alert.present()
  //   })
  // }



  
}
