import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { BookingService } from 'src/app/services/booking.service';
import { DoctorService } from 'src/app/services/doctor.service';
import { Observable } from 'rxjs';
import { bookingstruct } from 'src/app/modal/booking';
import { Doctorstruct } from 'src/app/modal/doctor';
import { AutoSlotUpdateService } from 'src/app/services/auto-slot-update.service';

@Component({
  selector: 'app-appointments',
  templateUrl: './appointments.page.html',
  styleUrls: ['./appointments.page.scss'],
})
export class AppointmentsPage implements OnInit {

  todaysDate = new Date().toLocaleDateString()
  bookingsdb1: Observable<bookingstruct[]>
  // doctorsdb : Observable<Doctorstruct>
  doctor : Doctorstruct ={
    name:'',
    description:'',
    department:'',
    phone: '',
    specialization: '',
    consultationFee : '',
    timings:'',
    facilities:'',
    createdAt: '',
    // docId : '',
  }

  
  did:string;
  uid:string;
  constructor(private fbAutoSlotUpdateService : AutoSlotUpdateService,private alertCtrl:AlertController ,private router:Router,private fbBookingService:BookingService,private fbDoctorService : DoctorService,private navCtrl:NavController,private activatedRoute : ActivatedRoute) 
  {
    this.did= this.activatedRoute.snapshot.paramMap.get('did')
    this.uid= this.activatedRoute.snapshot.paramMap.get('uid')

    if (this.did) {
      this.bookingsdb1 = this.fbBookingService.getBookingsInParticular(this.did)
      this.fbDoctorService.getDoctor(this.did).subscribe((docData)=>{
        this.doctor = docData
        this.fbAutoSlotUpdateService.updation(this.bookingsdb1,this.doctor);
      })
    }
  }

  ngOnInit() {
  }

  goBack()
  {
    this.navCtrl.back()
  }


  async updateBookingStatus(booking : bookingstruct)  {
    // console.log(new Date('04/01/2011'))
    // console.log(new Date('2011/04/01'))

    // console.log(new Date('04/02/2020'))

    // console.log(new Date('2011/04/01'))
    // console.log(new Date('2020/04/02'))

    // let y= this.fbTimeService.localeDateStringToConvertibleDateString('01/04/2011')
    // console.log(new Date(y));
    
    

    await this.alertCtrl.create({
      message: "are you sure to update status to VISITED or expired ",
      buttons: [
        { text: "cancel",
          handler: () => {
          }
        },
        {
          text: "ok",
          handler: async () => {
            this.doctor.pendingBookings = this.doctor.pendingBookings - 1  // first update pending bpokings and then ther itself in doctor service
                        // call the booking service which is just after this line , it is as done in hms user  app
          // await this.fbBookingService.updateBookingStatus(booking)
            await this.fbDoctorService.updateDoctorPendingBookings(this.doctor,booking)
          }
        }]
    }).then((alert) => {
      alert.present()
    })
  }



  open(bid: string){  // to open an appointment in a single page
    this.navCtrl.navigateForward(`firstloadpage/${this.uid}/appointments/${this.did}/open-single-appointments/${bid}`)
  }

}
