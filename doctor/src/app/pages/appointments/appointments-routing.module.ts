import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppointmentsPage } from './appointments.page';

const routes: Routes = [
  {
    path: '',
    component: AppointmentsPage
  //   ,
  //   children :[
  //     {
  //       path:'',
  //       children :[
  //         {
  //           path: 'open-single-appointments/:id',
  //           loadChildren: () => import('./open-single-appointments/open-single-appointments.module').then( m => m.OpenSingleAppointmentsPageModule)
  //         },
  //         {
  //           path: 'cancel-single-appointment',
  //           loadChildren: () => import('./cancel-single-appointment/cancel-single-appointment.module').then( m => m.CancelSingleAppointmentPageModule)
  //         }
  //       ]

  //     }
  //   ]
  },


  // {
  //   path: 'open-single-appointments/:id',
  //   loadChildren: () => import('./open-single-appointments/open-single-appointments.module').then( m => m.OpenSingleAppointmentsPageModule)
  // },

  // {
  //   path: 'cancel-single-appointment/:id',
  //   loadChildren: () => import('./cancel-single-appointment/cancel-single-appointment.module').then( m => m.CancelSingleAppointmentPageModule)
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AppointmentsPageRoutingModule {}
