import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OpenSingleAppointmentsPage } from './open-single-appointments.page';

const routes: Routes = [
  {
    path: '',
    component: OpenSingleAppointmentsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OpenSingleAppointmentsPageRoutingModule {}
