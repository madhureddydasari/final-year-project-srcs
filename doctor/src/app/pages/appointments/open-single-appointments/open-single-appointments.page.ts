import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { bookingstruct } from 'src/app/modal/booking';
import { BookingService } from 'src/app/services/booking.service';
import { NavController, AlertController } from '@ionic/angular';
import { DoctorService } from 'src/app/services/doctor.service';
import { Doctorstruct } from 'src/app/modal/doctor';
  
@Component({
  selector: 'app-open-single-appointments',
  templateUrl: './open-single-appointments.page.html',
  styleUrls: ['./open-single-appointments.page.scss'],
})
export class OpenSingleAppointmentsPage implements OnInit {

  todaysDate = new Date().toLocaleDateString()
  bookingsdb : Observable<bookingstruct[]>
  doctor: Doctorstruct={
    // dateRange?: number,
    // id?: any,
    name: "",
    department:'',
    phone : '',
    description : '',
    specialization: '',
    consultationFee : '',
    timings:'',
    facilities: '',
    createdAt :'',
    // timeSlots?: any[],
  }


  constructor(private fbBookingService:BookingService,private activatedRoute : ActivatedRoute,private navCtrl:NavController,private alertCtrl:AlertController,private fbServiceDoctor : DoctorService) 
  {
    let id=this.activatedRoute.snapshot.paramMap.get('bid')
    this.bookingsdb=this.fbBookingService.getSingleBooking(id)

    let did=this.activatedRoute.snapshot.paramMap.get('did')
    this.fbServiceDoctor.getDoctor(did).subscribe((docData)=>{ this.doctor = docData})

  }

  ngOnInit() {
  }

  goBack()
  {
    this.navCtrl.back();
  }

  async cancelBooking(book: bookingstruct)
  {
    this.alertCtrl.create({
      message:"are u sure to cancel booking",
      buttons:[{text:"yes",handler:()=> this.doCancelBooking(book)},{text:"no"}]
    }).then((alert) => { alert.present() })
  }

  

  async doCancelBooking(book: bookingstruct){
    document.getElementById('hidden-div').style.display = 'block'; 
    document.getElementById('hidden-div').style.display = 'none'


    console.log(book.doctorID)  // working fine
    console.log(this.doctor.id)    // not working //now its working fine

    let sId=book.slotId;
    let dayOfTimeSlots = Math.floor(sId/1000)-1;
    let slotOfTimeSlots = sId%1000-1;
    let sDate=book.slotDate
    let array=await this.doctor.timeSlots[dayOfTimeSlots].slots[slotOfTimeSlots].slotDateArray
    let index = array.indexOf(sDate)
    // delete array[index] 
    // await array.pop()
    await this.doctor.timeSlots[dayOfTimeSlots].slots[slotOfTimeSlots].slotDateArray.splice(index, 1);
    // this.fbServiceDoctor.updateDoctor(this.doctor)
    this.doctor.pendingBookings-=1
    this.fbServiceDoctor.updateDoctorById(book,this.doctor)
    await this.fbBookingService.cancelBooking(book);//

  }

  async updateBookingStatus(booking : bookingstruct)  {
    // console.log(new Date('04/01/2011'))
    // console.log(new Date('2011/04/01'))

    // console.log(new Date('04/02/2020'))

    // console.log(new Date('2011/04/01'))
    // console.log(new Date('2020/04/02'))

    // let y= this.fbTimeService.localeDateStringToConvertibleDateString('01/04/2011')
    // console.log(new Date(y));
    
    

    await this.alertCtrl.create({
      message: "are you sure to update status to VISITED or expired ",
      buttons: [
        { text: "cancel",
          handler: () => {
          }
        },
        {
          text: "ok",
          handler: async () => {
            this.doctor.pendingBookings = this.doctor.pendingBookings - 1  // first update pending bpokings and then ther itself in doctor service
                        // call the booking service which is just after this line , it is as done in hms user  app
          // await this.fbBookingService.updateBookingStatus(booking)
            await this.fbServiceDoctor.updateDoctorPendingBookings(this.doctor,booking)
          }
        }]
    }).then((alert) => {
      alert.present()
    })
  }



}
