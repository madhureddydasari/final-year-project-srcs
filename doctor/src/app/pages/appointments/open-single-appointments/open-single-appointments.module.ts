import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OpenSingleAppointmentsPageRoutingModule } from './open-single-appointments-routing.module';

import { OpenSingleAppointmentsPage } from './open-single-appointments.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OpenSingleAppointmentsPageRoutingModule
  ],
  declarations: [OpenSingleAppointmentsPage]
})
export class OpenSingleAppointmentsPageModule {}
