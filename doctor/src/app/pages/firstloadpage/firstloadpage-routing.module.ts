import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FirstloadpagePage } from './firstloadpage.page';

const routes: Routes = [
  {
    path: '',
    component: FirstloadpagePage,
  // },
    children:[
      {
        path: '',
        children :[
          {
            path:'home/:uid',
            children:
            [
              {
                path : '',
                loadChildren: () => import('../home/home.module').then( m => m.HomePageModule)
              },

              // 

            ]
          },


      ]
      },
    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FirstloadpagePageRoutingModule {}
