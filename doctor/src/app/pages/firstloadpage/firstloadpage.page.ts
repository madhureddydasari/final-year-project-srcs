import { Component, OnInit } from '@angular/core';
import { AlertController, NavController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { DoctorService } from 'src/app/services/doctor.service';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Doctorstruct } from 'src/app/modal/doctor';

@Component({
  selector: 'app-firstloadpage',
  templateUrl: './firstloadpage.page.html',
  styleUrls: ['./firstloadpage.page.scss'],
})
export class FirstloadpagePage implements OnInit {
id1 : string;
  pages = [
    {
      title : "Appointments",
      url:"appointments/"
    },
    {
      title: " history",
      url:"history/"
    }
  ];

  doctorsdb : Observable<Doctorstruct[]>


  constructor(private fbDoctorService : DoctorService,private activatedRoute : ActivatedRoute,private Aauth : AngularFireAuth,private alertCtrl :AlertController , private navCtrl : NavController)
  {
    this.id1 = this.activatedRoute.snapshot.paramMap.get('uid')
    if(this.id1)
    {
      this.doctorsdb = this.fbDoctorService.getDoctorsInParticular(this.id1)
      // this.doctorsdb.subscribe((result) => { this.singledoctor = result[0] })
    }

  }

  ngOnInit() {
  }

  // logout()
  // {
  //   this.alertCtrl.create({
  //     message:"Are u sure , u want to logout",
  //     buttons : [{text :"cancel"},
  //               { text : "ok",
  //                 handler : ()=> {
  //                   this.Aauth.auth.signOut().then(()=>{
  //                     this.navCtrl.navigateRoot("login")
  //                   })
  //                 }
  //             }]
  //   }).then((alert)=>{ alert.present() })
    

  // }

}
