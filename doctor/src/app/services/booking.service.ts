

import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore , DocumentReference } from '@angular/fire/firestore';
import { bookingstruct } from 'src/app/modal/booking'
import { Observable } from 'rxjs';
import {  map, take} from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';
// import { AngularFireDatabase } from "@angular/fire/database";
// import { promise, $ } from 'protractor';
import { ToastController, NavController } from '@ionic/angular';
import { StrToDateConversionService } from './str-to-date-conversion.service';


@Injectable({
  providedIn: 'root'
})
 
export class BookingService {
  
  bookingsdb : Observable<bookingstruct[]>
  bookingCollection : AngularFirestoreCollection<bookingstruct>

  constructor(private fbTimeService :StrToDateConversionService,private navCtrl : NavController,private toastCtrl : ToastController, private afs : AngularFirestore , private Aauth : AngularFireAuth) 
  { 
  }

//gives ouput as all bookings of a particular doctor
  getBookingsInParticular(currentDoctorID: string) : Observable<bookingstruct[]>
  {         
        this.bookingCollection = this.afs.collection<bookingstruct>('BookingsList' , ref => ref.where('doctorID','==',currentDoctorID).orderBy('bookingDate','desc')) ;
        this.bookingsdb = this.bookingCollection
        .snapshotChanges().pipe(
          map(actions =>{
            return actions.map(a=> {
              const data = a.payload.doc.data();
              const id = a.payload.doc.id;
              return { id, ...data };
            });
          })
        )
        return this.bookingsdb;
  }

  // gives a particular booking details by its booking id
  getSingleBooking(bookingId: string) : Observable<bookingstruct[]>
  {         
        this.bookingCollection = this.afs.collection<bookingstruct>('BookingsList' , ref => ref.where('bookingId','==',bookingId) ) ;
        this.bookingsdb = this.bookingCollection
        .snapshotChanges().pipe(
          map(actions =>{
            return actions.map(a=> {
              const data = a.payload.doc.data();
              const id = a.payload.doc.id;
              return { id, ...data };
            });
          })
        )
        return this.bookingsdb;
  }


  async cancelBooking(booking:bookingstruct)
  {
    // if(!booking.reason)
    // {
    //   booking.reason=""
    // }
    await this.bookingCollection.doc(booking.id).update({
      status:"cancelled",
      statusUpdatedBy:"user",
      reason:booking.reason,
    }).then(()=>{ this.navCtrl.back()})
  }

  autoUpdateBookingStatus(bookingId:string)
  {
    this.bookingCollection =this.afs.collection<bookingstruct>('BookingsList')
    return this.bookingCollection.doc(bookingId).set({status:"expired"},{merge:true})
    
  }
  
  // updates particular booking status
  async updateBookingStatus(booking: bookingstruct)
  {
    this.bookingCollection =this.afs.collection<bookingstruct>('BookingsList')
    const todaysDate = new Date().toLocaleDateString()

    let date1=this.fbTimeService.localeDateStringToDate(todaysDate)
    let date2=this.fbTimeService.localeDateStringToDate(booking.slotDate)

    console.log("date 1 and date2 are : "+date1 + date2);
    

    
    if(date1 = date2)
    {
      return this.bookingCollection.doc(booking.id).set({status:"visited"},{merge:true})
    }
    else if(date1 > date2){
      return this.bookingCollection.doc(booking.id).set({status:"expired"},{merge:true})
    }
  }

  // getbooking(id: string) : Observable<bookingstruct>{
  //   return this.bookingCollection.doc<bookingstruct>(id).valueChanges().pipe(
  //     take(1),
  //     map(firestoreDocumentValue => {
  //       firestoreDocumentValue.id = id;
  //       return firestoreDocumentValue;
  //     })
  //   );
  // }


  // bookDoctor(book : bookingstruct) 
  // {
  //         let uid = this.Aauth.auth.currentUser.uid ;
  //         book.userUID = uid ;
  //         return this.bookingCollection.add(book)
  //         .then(ref => {
  //           ref.set({ bookingId : ref.id},{merge : true})
  //           // merege says that add field bookingId and everything else is same as before
  //           .then(() => {
  //             // console.log("add aextra field also")
  //             this.toastCtrl.create({
  //               message : "booking id is :"+ ref.id,
  //               duration : 2000
  //             }).then((toast) => {
  //               toast.present()
  //             })
  //           })
  //         })
    
  //   }
}
 