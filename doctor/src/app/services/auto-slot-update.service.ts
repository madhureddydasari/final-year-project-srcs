import { Injectable } from '@angular/core';
import { StrToDateConversionService } from './str-to-date-conversion.service';
import { BookingService } from './booking.service';
import { Observable } from 'rxjs';
import { bookingstruct } from '../modal/booking';
import { Doctorstruct } from '../modal/doctor';
import { DoctorService } from './doctor.service';

@Injectable({
  providedIn: 'root'
})
export class AutoSlotUpdateService {

  todaysDate : string = new Date().toLocaleDateString()
  constructor(private fbTimeService : StrToDateConversionService , private fbBookingService : BookingService,private fbDoctorService : DoctorService) { }

  updation(bookingsdb1 : Observable<bookingstruct[]>,doctor : Doctorstruct)
  {
    // console.log(doctor);
    
    bookingsdb1.subscribe((docdata) =>{ 
      // console.log(docdata);
      for(let i=0 ; i<docdata.length ; i++)
      {
        if(docdata[i].status == 'unvisited')
        {
          if(this.convertToDate(docdata[i].slotDate) < this.convertToDate(this.todaysDate))
          {
            doctor.pendingBookings--
            this.fbBookingService.autoUpdateBookingStatus(docdata[i].id)
            this.fbDoctorService.updateDoctorById(docdata[i],doctor)
          }
        }
      }
     }) ;
  }

  convertToDate(x: string)
  {
    return this.fbTimeService.localeDateStringToDate(x)
  }

}
