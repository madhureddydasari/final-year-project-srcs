import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { Doctorstruct } from "../modal/doctor";
import { map, take } from "rxjs/operators";
import { AngularFirestoreCollection, AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { ToastController, NavController } from '@ionic/angular';
import { bookingstruct } from '../modal/booking';
import { BookingService } from './booking.service';

@Injectable({
  providedIn: 'root'
})
export class DoctorService {

  private doctorsdb: Observable<Doctorstruct[]>
  private doctorCollection: AngularFirestoreCollection<Doctorstruct>;
  displayy:string
  constructor(private afs : AngularFirestore , private fbBookingService:BookingService) 
  {


  }


    // to retrieve particular doctor details by doctor id
    getDoctor(id: string) : Observable<Doctorstruct>{
    this.doctorCollection=this.afs.collection<Doctorstruct>('DoctorsList');
      return this.doctorCollection.doc<Doctorstruct>(id).valueChanges().pipe(
        take(1),
        map(firestoreDocumentValue => {
          firestoreDocumentValue.id = id;
          return firestoreDocumentValue;
        })
      );
    }

  getDoctorsInParticular(activatedId :string)  
  {
    this.doctorCollection=this.afs.collection<Doctorstruct>('DoctorsList',ref => ref.where("doctuserUID","==",activatedId) );
    this.doctorsdb = this.doctorCollection.snapshotChanges().pipe(
      map(actions =>{
        return actions.map(a=> {
           const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    )
    return this.doctorsdb;
  }

  async updateDoctor(doctor : Doctorstruct)
  {
    await this.doctorCollection.doc(doctor.id).set({toInclude :!doctor.toInclude},{merge :true}).then(()=> {
      if(doctor.toInclude == false){
        this.displayy="none"
      }
      else if(doctor.toInclude == true){
        this.displayy="block"
      }
      document.getElementById('abcde').style.display=this.displayy;
    
    })


   
  }

  async updateDoctorPendingBookings(doctor:Doctorstruct , booking :bookingstruct){
    await this.doctorCollection.doc(doctor.id).update({pendingBookings : doctor.pendingBookings})
    .then(async ()=>{
      await this.fbBookingService.updateBookingStatus(booking)
    })
  }


  async updateDoctorById(booking: bookingstruct ,doctor: Doctorstruct)
  {
    await this.doctorCollection.doc(doctor.id)
      .set({ timeSlots: doctor.timeSlots , pendingBookings : doctor.pendingBookings }, { merge: true });
    // this.fbBookingService.cancelBooking(booking);
  }

}
