import { TestBed } from '@angular/core/testing';

import { AutoSlotUpdateService } from './auto-slot-update.service';

describe('AutoSlotUpdateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AutoSlotUpdateService = TestBed.get(AutoSlotUpdateService);
    expect(service).toBeTruthy();
  });
});
