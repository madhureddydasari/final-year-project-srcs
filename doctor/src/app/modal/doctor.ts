export interface Doctorstruct {

    doctorExist ?:boolean;
    // startTime?:string;
    // endTime?:string;
    id?: any;
    name: string;
    description : string;
    department : string;
    phone : string;
    specialization: string;
    consultationFee : string;
    timings:string;
    facilities: string;
    createdAt? :any;
    lastModified? :any ;
    docId ?: any;
    timeSlots?: any[];   // stores particular users timeslots
    doctuserUID?:string;
    emailId ?: string;
    password ?: string;
    pendingBookings?:number;
    toInclude ?: boolean;  // given in doctregister.ts "yes" by default while adding for first time


} 
