import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DoctregisterPage } from './doctregister.page';

describe('DoctregisterPage', () => {
  let component: DoctregisterPage;
  let fixture: ComponentFixture<DoctregisterPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoctregisterPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DoctregisterPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
