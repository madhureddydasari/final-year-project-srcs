import { Component } from '@angular/core';
import { FirebaseService } from "../services/firebase.service";
import { Observable } from 'rxjs';
import { Doctorstruct } from '../modal/Docotor';
import { AlertController, NavController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { timestamp } from 'rxjs/operators';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  // doctor : Doctorstruct ={
  //   name:'',
  //   description:'',
  //   department:'',
  //   phone: '',
  //   specialization: '',
  //   consultationFee : '',
  //   timings:'',
  //   facilities:'',
  //   createdAt: '',
  //   // docId : '',
  // } 

  doctorsdb : Observable<Doctorstruct[]>
  doctorsdb1 : Observable<Doctorstruct[]>
  doctorsdb2 : Observable<Doctorstruct[]>

  constructor(private afAuth : AngularFireAuth,private navCtrl : NavController,private alertCtrl:AlertController,private fbservice : FirebaseService) 
  {

  }
  ngOnInit(){
    this.afAuth.auth.onAuthStateChanged((user) => {
      if(user)
      {
        
      }
      else{
        this.navCtrl.navigateRoot('/developerlogin');
      }
    })
    this.doctorsdb=this.fbservice.getDoctors();

    this.doctorsdb1=this.fbservice.getActiveDoctors();
    this.doctorsdb2=this.fbservice.getInActiveDoctors();

  }

  // change(x :Doctorstruct){
  //   this.fbservice.enableDisableDoctor(x)
  // }
  
  changeActiveToBlock(x : Doctorstruct)
  {
    this.alertCtrl.create({
      message : "are u sure to block dis doctor",
      buttons : [
        {
          text : "Cancel",
          handler : () => {
            x.doctorExist = true
            this.fbservice.enableDisableDoctor(x) 
          }
        },{
          text : "OK",
          handler : () => {
            x.doctorExist = false
          this.fbservice.enableDisableDoctor(x) 
          }
        }
      ]
  }).then((alert) => alert.present())

  }

  


  changeBlockToActive(x:Doctorstruct)
  {
    this.alertCtrl.create({
      message : "are u sure to Allow dis doctor",
      buttons : [
        {
          text : "Cancel",
          handler : () => {
            x.doctorExist = false
            this.fbservice.enableDisableDoctor(x) 
          }
        },{
          text : "OK",
          handler : () => {
            x.doctorExist = true;
          this.fbservice.enableDisableDoctor(x) 
          }
        }
      ]
  }).then((alert) => alert.present())

    
  }


  logout(){
    this.alertCtrl.create({
      message : "are u sure to Allow dis doctor",
      buttons : [
        {
          text : "Cancel",

        },
        {
          text : "OK",
          handler : () => {
            this.afAuth.auth.signOut()
            }
        }
      ]
  }).then((alert) => alert.present())

  }


 
  printData(timestamp : firebase.firestore.Timestamp)
  {
    let date = timestamp.toDate();
    return "createdOn :"+ date.toLocaleDateString()
    // return "doctor createdAt1 date :"+date.toLocaleDateString()
  }
}



 