export interface Doctorstruct {
        doctorExist ?:boolean; // to active or block the doctor
        id?: any;
        name: string;
        description : string;
        department : string;
        phone : string;
        specialization: string;
        consultationFee : string;
        timings:string;
        facilities: string;
        createdAt? :any;    // new Date().getTime()   // collects in milliseconds
        createdOn? :Date;    // new Date()    // collects entire date and can be viewed by convertng to date.toLocalDateString
        lastModified? :any ;  // 
        docId ?: any;
        doctuserUID?:string;  // doctors user UID
        emailId ?: string;
        password ?: string;
        pendingBookings?:number;
        dateRange?:number; // it is useful for a doctor to show dates available for booking on calendar using no.of days
        toInclude ?: boolean;  // Doctors Choice to apper or disapper himself to the user
        timeSlots?: any[];   // stores particular users timeslots
        startTimings?:string[];  // input taken from user of weekly startTimes
        endTimings?:string[]  ;    // input taken from user of weekly  endTimes
        timingIntervals?:number[]; // input taken from user of weekly  timeIntervals Between startTimes nd endTime
        // given in doctregister.ts "yes" by default while adding for first time
    } 

    export interface bookingstruct {
        id? : any; // saves id of the booking document
        userPatientListDocId? : any; //saves user's i.e.patients document id
        userFullName? : any; //stores usersFullName in each booking document to know which user/ name of the  created the booking
        bookingId? : string; // generates a unique booking id for each booking
        doctorID? : string ; // stores the doctor id of the doctor to whom the Appointment has been requested 
        doctorName? : string; // stores doctorname and saves in every document for which doctor the booking has been done
        bookingDate? : Date;  // stores date on which the booking has been done
        userUID? : string; // stores user's UID for further use
        status? : string; // says the status of the booking i.e.either VISITED or UNVISITED
        statusUpdatedBy?:string;
        reason?:string;
        // bookedSlot?:any[]; //stores details of any booked slot
        slotId?:any;
        slotDate?:any;
        slotTime?:any;    
    }

// export interface bookingstruct {
//     id?: any;
//     userID : string ;
//     doctorid : string ;
//     doctorname : string ;
//     status : string ;
// }