import { TestBed } from '@angular/core/testing';

import { StrToDateConversionService } from './str-to-date-conversion.service';

describe('StrToDateConversionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StrToDateConversionService = TestBed.get(StrToDateConversionService);
    expect(service).toBeTruthy();
  });
});
