import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { bookingstruct } from 'src/app/modal/Docotor';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BookingService {

  bookingCollection : AngularFirestoreCollection<bookingstruct>
  bookingsdb : Observable<bookingstruct[]>


  constructor( private afs : AngularFirestore) { }

  getBookings(currentDoctorId: string) : Observable<bookingstruct[]>
  {
            // define data base in collection variable i.e. stores collection name for suppose
                // to find current user and get details ie. bookings of only that user based on userId of login user122222113333333333 ref => ref.where("toInclude","==",true) && ref.where("doctorExist","==",true)
                // and the userUID (i.e. uid of signedinuser )that is stored while creating
        this.bookingCollection = this.afs.collection<bookingstruct>('BookingsList' , ref => ref.where('doctorID','==',currentDoctorId).orderBy('bookingDate','desc')) ;
// this is to be prsent in constructor but we place here because it does load before the id retrieved from firebase
        this.bookingsdb = this.bookingCollection
        .snapshotChanges().pipe(
          map(actions =>{
            return actions.map(a=> {
              const data = a.payload.doc.data();
              const id = a.payload.doc.id;
              return { id, ...data };
            });
          })
        ) 
        return this.bookingsdb;
  }

  autoUpdateBookingStatus(bookingId:string)
  {
    this.bookingCollection =this.afs.collection<bookingstruct>('BookingsList')
    return this.bookingCollection.doc(bookingId).set({status:"expired"},{merge:true})
    
  }
  
}
