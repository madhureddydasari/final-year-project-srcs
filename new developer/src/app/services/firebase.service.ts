import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import * as firebase from "firebase";
import { Doctorstruct, bookingstruct, } from "../modal/Docotor";
import { map, take } from "rxjs/operators";
import { AngularFirestoreCollection, AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { ToastController, NavController } from '@ionic/angular';


@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

  tempId :string;
  private doctorsdb: Observable<Doctorstruct[]>
  private doctorsdb1: Observable<Doctorstruct[]> //to collection doctors who are not blocked
  private doctorsdb2: Observable<Doctorstruct[]> //to collection doctors who are blocked
// this helps in allowing or not Allowing Doctors to take appintments 
  private doctorCollection: AngularFirestoreCollection<Doctorstruct>;
  private doctorCollection1: AngularFirestoreCollection<Doctorstruct>; // using where condition to collect unblocked doctors data
  private doctorCollection2: AngularFirestoreCollection<Doctorstruct>; // using where condition to collect blocked doctors data
  private doctorCollection3: AngularFirestoreCollection<Doctorstruct>;

  // private bookingCollection: AngularFirestoreCollection<bookingstruct>;


  constructor( private navCtrl : NavController,private toastCtrl : ToastController,private afs : AngularFirestore ,private Auth : AngularFireAuth) 
  {
    this.doctorCollection=this.afs.collection<Doctorstruct>('DoctorsList');
    this.doctorCollection1=this.afs.collection<Doctorstruct>('DoctorsList',ref => ref.where("doctorExist","==",true));
    this.doctorCollection2=this.afs.collection<Doctorstruct>('DoctorsList',ref => ref.where("doctorExist","==",false));

   
    this.doctorsdb = this.doctorCollection.snapshotChanges().pipe(
      map(actions =>{
        return actions.map(a=> {
           const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    )

    // to collection doctors who are unblocked
    this.doctorsdb1 = this.doctorCollection1.snapshotChanges().pipe(
      map(actions =>{
        return actions.map(a=> {
           const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    )

    // to collection doctors who are blocked
    this.doctorsdb2 = this.doctorCollection2.snapshotChanges().pipe(
      map(actions =>{
        return actions.map(a=> {
           const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    )


   } //end of Constructor

 
  getDoctors() : Observable<Doctorstruct[]>{
     return this.doctorsdb;
   }

  getActiveDoctors() : Observable<Doctorstruct[]>{
    return this.doctorsdb1; //gets unblocked doctor available doctors
  }  
  getInActiveDoctors() : Observable<Doctorstruct[]>{
    return this.doctorsdb2; //gets blocked doctor available doctors
  }

   
// get any particular deoctor details using the doctor id
   getDoctor(idd: string) : Observable<Doctorstruct>{
     return this.doctorCollection.doc<Doctorstruct>(idd).valueChanges().pipe(
       take(1),
       map(note=> {
         note.id=idd;
         return note;
       })
     );
   }

   getSnapShotSingleDoctor(id: string){

    this.doctorCollection3=this.afs.collection<Doctorstruct>('DoctorsList' ,ref=> ref.where("docId",'==',id));
    this.doctorsdb = this.doctorCollection3.snapshotChanges().pipe(
      map(actions =>{
        return actions.map(a=> {
           const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    )

    return this.doctorsdb;
    
  }

   
// to add a new doctor i.e. to to regster a new doctor
  async addDoctor(doctor: Doctorstruct) 
  {
      return await this.doctorCollection.add(doctor).then(ref => {
        this.tempId=ref.id;
        ref.set({ docId : ref.id }, { merge: true }) })// if we dont use merge remaining fields will vanish
        .then(() => this.showToast("doctor successfully added but hidden")  ) 
        .then(async () => {await this.navCtrl.navigateForward(`/update-doctor/${this.tempId}`) })  
        // .then(() => { this.Auth.auth.signOut() })
         
  } 

// to block or unblock the doctor by reversing the boolean value of doctorExist
  enableDisableDoctor(doctor :Doctorstruct)
  {
    return this.doctorCollection.doc(doctor.id).update({doctorExist : doctor.doctorExist})
  }


  
  // to update particular doctors details
   async updateDoctor(doctor: Doctorstruct) : Promise<void>{
    await this.doctorCollection.doc(doctor.id).update({
       lastModified: new Date(),
       dateRange: doctor.dateRange,
       name: doctor.name,
       department: doctor.department,
       description: doctor.description,
       phone: doctor.phone,
       specialization: doctor.specialization,
       consultationFee: doctor.consultationFee,
       doctorExist: doctor.doctorExist,
       timings : doctor.timings , 
       facilities : doctor.facilities, 
     }).then(()=>{
      this.showToast("doctor details successfully updated");
     })
                                                      }


// no need to delete a Doctor , because his details helps the patients  to track theit bookings

  // deleteDoctor(id:string): Promise<void> {
  //   return this.doctorCollection.doc(id).delete();
  // }

  async updateDoctorSlots(doctor:Doctorstruct): Promise<void>
  {
    if(doctor.pendingBookings == 0)
    {
      await this.doctorCollection.doc(doctor.id).update({
        startTimings:doctor.startTimings,
        endTimings:doctor.endTimings,
        timingIntervals:doctor.timingIntervals,
        timeSlots:doctor.timeSlots
      }).then(()=>{
        this.showToast("doctor details successfully updated");
       })
    }

    else{
      this.showToast("please clear pending Bookings or cancel them to update time slots")
    }

  }

  async updateDoctorById(booking: bookingstruct ,doctor: Doctorstruct)
  {
    await this.doctorCollection.doc(doctor.id)
      .set({ timeSlots: doctor.timeSlots , pendingBookings : doctor.pendingBookings }, { merge: true });
    // this.fbBookingService.cancelBooking(booking);
  }


  async showToast(message : string){
    const toast =await this.toastCtrl.create({
      message ,
      duration : 2000
    })
    toast.present()
  }

}
 