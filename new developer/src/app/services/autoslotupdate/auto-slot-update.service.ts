import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { bookingstruct, Doctorstruct } from 'src/app/modal/Docotor';
import { BookingService } from '../bookingService/booking.service';
import { StrToDateConversionService } from '../strToDateConversion/str-to-date-conversion.service';
import { FirebaseService } from '../firebase.service';

@Injectable({
  providedIn: 'root'
})
export class AutoSlotUpdateService {

  todaysDate : string = new Date().toLocaleDateString()
  constructor(private fbTimeService : StrToDateConversionService , private fbBookingService : BookingService , private fbservice:FirebaseService) { }


  updation(bookingsdb1 : Observable<bookingstruct[]>,doctor : Doctorstruct)
  {
    // console.log(doctor);
    
    bookingsdb1.subscribe((docdata) =>{ 
      // console.log(docdata);
      for(let i=0 ; i<docdata.length ; i++)
      {
        if(docdata[i].status == 'unvisited')
        {
          if(this.convertToDate(docdata[i].slotDate) < this.convertToDate(this.todaysDate))
          {
            doctor.pendingBookings--
            this.fbBookingService.autoUpdateBookingStatus(docdata[i].id)
            this.fbservice.updateDoctorById(docdata[i],doctor)
          }
        }
      }
     }) ;
  }

  convertToDate(x: string)
  {
    return this.fbTimeService.localeDateStringToDate(x)
  }

}
