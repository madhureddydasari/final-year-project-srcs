import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DeveloperloginPageRoutingModule } from './developerlogin-routing.module';

import { DeveloperloginPage } from './developerlogin.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DeveloperloginPageRoutingModule
  ],
  declarations: [DeveloperloginPage]
})
export class DeveloperloginPageModule {}
