import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UpdateDoctorSlotsPageRoutingModule } from './update-doctor-slots-routing.module';

import { UpdateDoctorSlotsPage } from './update-doctor-slots.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UpdateDoctorSlotsPageRoutingModule
  ],
  declarations: [UpdateDoctorSlotsPage]
})
export class UpdateDoctorSlotsPageModule {}
