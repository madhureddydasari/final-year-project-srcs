import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: '/index/decider', pathMatch: 'full' },
  {
    path: 'index',
    loadChildren: () => import('./pages/index/index.module').then( m => m.IndexPageModule)
  },
  {
    path: 'sidemenu',
    loadChildren: () => import('./pages/sidemenu/sidemenu.module').then( m => m.SidemenuPageModule)
  },

  {
    path: 'viewprofile/:id',
    loadChildren: () => import('./pages/sidemenu/viewprofile/viewprofile.module').then( m => m.ViewprofilePageModule)
  },
  
  {
    path: 'help',
    loadChildren: () => import('./pages/sidemenu/help/help.module').then( m => m.HelpPageModule)
  },
  {
    path: 'about',
    loadChildren: () => import('./pages/sidemenu/about/about.module').then( m => m.AboutPageModule)
  },
  {
    path: 'history',
    loadChildren: () => import('./pages/sidemenu/history/history.module').then( m => m.HistoryPageModule)
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
