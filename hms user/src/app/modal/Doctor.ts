export interface Doctorstruct {
    dateRange?: number;
    id?: any;
    name: string;
    department:'';
    phone : string;
    description : string;
    specialization: string;
    consultationFee : string;
    timings:string;
    facilities: string;
    createdAt :any;
    timeSlots?: any[];
    pendingBookings?:number;
 
}

export interface bookingstruct {
    id? : any; // saves id of the booking document
    userPatientListDocId? : any; //saves user's i.e.patients document id
    userFullName? : any; //stores usersFullName in each booking document to know which user/ name of the  created the booking
    bookingId? : string; // generates a unique booking id for each booking
    doctorID? : string ; // stores the doctor id of the doctor to whom the Appointment has been requested 
    doctorName? : string; // stores doctorname and saves in every document for which doctor the booking has been done
    bookingDate? : Date;  // stores date on which the booking has been done
    userUID? : string; // stores user's UID for further use
    status? : string; // says the status of the booking i.e.either VISITED or UNVISITED
    statusUpdatedBy?:string;
    reason?:string;
    // bookedSlot?:any[]; //stores details of any booked slot
    slotId?:any;
    slotDate?:any;
    slotTime?:any;



    
}