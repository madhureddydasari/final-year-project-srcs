import { Component, OnInit } from '@angular/core';

import { AlertController, NavController, ToastController } from '@ionic/angular'
import { Router } from '@angular/router';

import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import * as firebase from "firebase";

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  email : string= ""
  password : string = ""


  constructor(
    private toastCtrl : ToastController,
    private navCtrl : NavController,
    public afAuth: AngularFireAuth,
    public alert : AlertController,
    public router :Router
    ) {

      // console.log("executed first in constructor")
//  will be executed first in constructor

      this.afAuth.auth.signOut();
      
      // this.afAuth.auth.onAuthStateChanged((user) => {
      //   if(user){
      //     console.log("userlogged in only in constructor")
      //   }
      //   else {
      //     console.log("userlogged Out in constructor")
      //   }
      // })

      // this.goFirstToThis();
     }

  ngOnInit() {
    // this.goFirstToThis()
    console.log("executed first in oninit")

    // this.afAuth.auth.signOut();
    // this.afAuth.auth.onAuthStateChanged((user) => {
    //   if(user){
    //     console.log("userlogged in only in onint")
    //   }
    //   else {
    //     console.log("userlogged Out in onint")
    //   }
    // })

  }
  // goFirstToThis(){
  //   this.afAuth.auth.signOut().then(() => {
  //     console.log()
  //   })
  // }
 

  async loginaction()
  {
    const { email,password } = this

    try{
      const res= await this.afAuth.auth.signInWithEmailAndPassword(email+'@usermail.com',password)
      .then(()=>{
        this.showToast("user successfully logged in");
        this.navCtrl.navigateRoot('/sidemenu/sidehome');
      })

    }
    catch(err){
      // if(err.code == "auth/invalid-email")
      // if(err.code == "auth/invalid-email")
      // {
        console.log("username and password doesnot match")
        // this.showAlert("Error!","Invalid Username and Password")
        this.showAlert("Error",err.message)
      // }
      
      // else{
      //   console.log(err.message)
      // this.router.navigate(['/index/login'])
      // this.showAlert("Error!","Invalid Username and Password")
      // }
      
    }

  }


  async showAlert(header :string , message :string)
  {
    const alerrt = await this.alert.create({
      header,
      message,
      buttons: ["OK"]
    })
    await alerrt.present()
  }
  showToast(message :string){
    this.toastCtrl.create({
      message,
      duration:2000,
    }).then((toast) => toast.present())
  }
}
 