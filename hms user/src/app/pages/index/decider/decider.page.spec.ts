import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DeciderPage } from './decider.page';

describe('DeciderPage', () => {
  let component: DeciderPage;
  let fixture: ComponentFixture<DeciderPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeciderPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DeciderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
