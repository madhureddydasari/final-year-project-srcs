import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';

@Component({
  selector: 'app-decider',
  templateUrl: './decider.page.html',
  styleUrls: ['./decider.page.scss'],
})
export class DeciderPage implements OnInit {

  constructor( private navCtrl : NavController , private router : Router,private Aauth : AngularFireAuth)
   { 
      //  this is to find if the user is logged in or not 
     
      this.Aauth.auth.onAuthStateChanged((user) => {
        if(user){
          console.log("user exists");
          this.navCtrl.navigateRoot('/sidemenu/sidehome');
        }
        else
        {
          console.log("user does not exists");
          this.navCtrl.navigateRoot('/index/welcome');
        }
        console.log("its done")
      })
    }

  ngOnInit() {  
  }

}
