import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import {AlertController, NavController, ToastController} from '@ionic/angular'
import { Router } from "@angular/router";
import { userStruct } from 'src/app/modal/patient';
// import { FirebasefirestoreService } from "src/app/services/firebasefirestore.service";
import { PatientService } from 'src/app/services/patientservice/patient.service';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
  userFullName:string=""
  email :string=""
  password :string=""
  cpassword : string=""

  patientdata : userStruct = {
    // lastBookingDate:null,  // no need to mention as it is declared globally in interface using ?
    limit:0,
    userFullName :"",
    userUID : "",
    emailId:"",
    userImageUrl : "",
    // createdON : null, // no need to mention here as it is altready given in structure or
}

    // id:string ;
  constructor(
    private toastCtrl : ToastController ,
    private patientService:PatientService,
    public afAuth: AngularFireAuth,
    public alert :AlertController,
    public router : Router,
    private navCtrl : NavController,
  ) { }


  ngOnInit() {
  }

  async registeraction() 
  {
    const {email ,password ,cpassword}=this
    if(email !== "")
    {
      if(password !== cpassword){
        this.showAlert("Error!","passwords doesnt match")
        return  console.log("passwords doesnt match")
      }

      try {
            await this.afAuth.auth.createUserWithEmailAndPassword(email+'@usermail.com' , password)
            .then(async (user)=>{  
              // this.patientdata.lastBookingDate=null ;
              this.patientdata.limit=0;
              this.patientdata.userFullName = this.userFullName ;
              this.patientdata.createdON = new Date();
              this.patientdata.userUID=user.user.uid ;
              this.patientdata.emailId=user.user.email ;
              await this.patientService.addPatient(this.patientdata) ;
            })
            .catch((err) => {
                // console.log(err) 
                this.showAlert("Warning",err.message)
              })

          }
      catch(error)
      {
        console.dir(error)
        this.showAlert("Error",error.message)
      }
    }
    else {
      this.showAlert("error","enter valid Email address")
    }

  }

  async showAlert(header :string , message :string)
  {
    const alert = await this.alert.create({
      header,
      message,
      buttons: ["OK"]
    })
    await alert.present()
  }


  showToast(message :string){
    this.toastCtrl.create({
      message,
      duration:2000,
    }).then((toast) => toast.present())
  }

}
 