import { Component, OnInit } from '@angular/core';
import { userStruct } from "src/app/modal/patient";
import { PatientService } from 'src/app/services/patientservice/patient.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-updateprofile',
  templateUrl: './updateprofile.page.html',
  styleUrls: ['./updateprofile.page.scss'],
})
export class UpdateprofilePage implements OnInit {

  patient : userStruct ={
    userFullName:'',
    emailId : '', //no edits
    userUID :'',   //no edits
    // createdON : null,
    userImageUrl : '',
  }
  id:string;
  constructor(private alertCtrl : AlertController,private navCtrl : NavController, private router : Router,private fbservicepatient : PatientService , private activateRoute : ActivatedRoute) { }

  ngOnInit() {
     this.id=this.activateRoute.snapshot.paramMap.get('id')
    if(this.id){
      this.fbservicepatient.getPatient(this.id).subscribe((docdata)=> {
        this.patient = docdata
      })
    }
  }

  updatePatient() {
    this.alertCtrl.create({
      message : "are u sure",
      buttons : [ { text:'cancel'} , {text :'ok' , handler :() => { this.fbservicepatient.updatePatient(this.patient) }}]
    }).then((alert) => { alert.present()})
  }
 
  goback(){
    // this.navCtrl.navigateBack(`/viewprofile/${this.id}`)
    this.navCtrl.back();
  }

}
