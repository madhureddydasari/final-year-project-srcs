// import { Component, OnInit } from '@angular/core';
// import { AlertController } from '@ionic/angular';
// import { Router } from '@angular/router';

// @Component({
//   selector: 'app-home',
//   templateUrl: './home.page.html',
//   styleUrls: ['./home.page.scss'],
// })
// export class HomePage implements OnInit {

//   doctorsdb: any[];
//   constructor(public alertCtrl: AlertController,private router : Router) 
//   { 
//     this.initializeItems();
//   }


//   ngOnInit() {
//   }

//   initializeItems(){
//     this.doctorsdb =[
//       {
//         name :"madhu Reddy",
//         department :"Dermatology",
//         description :"does nothing",
//         address : "mvp colony , visakhapatnam",
//         contact: "1234567890",
//         id:'1'
//       },
//       {
//         name :"Harsha",
//         department :"Orthology",
//         description :"Does nothing",
//         address : "mvp colony , visakhapatnam",
//         contact: "1234567890",
//         id:'2'
//       },
  
//       {
//         name :"Navya",
//         department :"Pediatrician",
//         description :"Does something",
//         address : "mvp colony , visakhapatnam",
//         contact: "1234567890",
//         id:'3'
//       },
  
//       {
//         name :"Reshma",
//         department :"Cardiologist",
//         description :"Deals with human hearts",
//         address : "mvp colony , visakhapatnam",
//         contact: "1234567890",
//         id:'4'
//       },
//       {
//         name :"madhu Reddy1",
//         department :"Gynocologist",
//         description :"does nothing",
//         address : "mvp colony , visakhapatnam",
//         contact: "1234567890",
//         id:'5'
//       },
//       {
//         name :"madhu Reddy2",
//         department :"ENT",
//         description :"does nothing",
//         address : "mvp colony , visakhapatnam",
//         contact: "1234567890",
//         id:'6'
//       },
      
//     ];
//   }

//   //Searchbar inptut is filtered here

//   getItems(ev: { target: { value: any; }; }) 
//   {
//        // Reset items back to all of the items
//        this.initializeItems();
//        // set val to the value of the ev target
//        var val = ev.target.value;
//        if (val && val.trim() != '') {
//         this.doctorsdb = this.doctorsdb.filter((p) => {
//           return ( ((p.department).toLowerCase().indexOf(val.toLowerCase()) > -1) || ((p.name).toLowerCase().indexOf(val.toLowerCase()) > -1)  || ((p.description).toLowerCase().indexOf(val.toLowerCase()) > -1) || ((p.address).toLowerCase().indexOf(val.toLowerCase()) > -1)  );
//         })
//       }
//     }
  


//   async selectdoctor(eve)
//   {

//     let k=eve.id;
//     // this.showAlert("Dr. "+eve.name , eve.department ,eve.description);
//     // this.showAlert("Dr. "+eve.name , eve.department ,k);

//     this.router.navigate([`/sidemenu/sidehome/tabs/home/${k}`]);
//     // this.showAlert();
//   }
 

//   async showAlert(header :string ,subHeader :string , message :string , )
//   // async showAlert()
//   {
//       const alert = await this.alertCtrl.create({
//         // header:"called selectdoctor function",
//         // message: "dont do that again",
//         // buttons :["ok"]
//         header,
//         subHeader,
//         message,
//         buttons:[{text:"Cancel"},{text:'Book Appointment'}]
        
//       });
//       await alert.present()
//   }
// }



//   // const searchbar = document.querySelector('ion-searchbar');
//   // const items = Array.from(document.querySelector('ion-list').children);
//   // searchbar.addEventListener('ionInput', handleInput);

//   // function handleInput(event) {
//   //   const query = event.target.value.toLowerCase();

//   //   requestAnimationFrame(() => {
//   //     items.forEach(item => {
//   //       const shouldShow = item.textContent.toLowerCase().indexOf(query) > -1;
//   //       item.style.display = shouldShow ? 'block' : 'none';
//   //     });
//   //   });
//   // }





import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
// import { FirebasefirestoreService } from 'src/app/services/firebasefirestore.service';
import {  DoctorService} from "src/app/services/firebasedoctor/doctor.service";
import { Observable } from 'rxjs';
import { Doctorstruct } from 'src/app/modal/Doctor';
import { filter } from 'rxjs/operators';


@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  doctorsdb: Observable<Doctorstruct[]>;
  constructor(public alertCtrl: AlertController,private router : Router , private fbservicedoctor :DoctorService) 
  { 
    
  }

  ngOnInit() {
    this.doctorsdb = this.fbservicedoctor.getAllDoctors();
  }
  


}


