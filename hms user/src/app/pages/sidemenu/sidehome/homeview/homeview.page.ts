// import { Component, OnInit } from '@angular/core';
// import { ActivatedRoute } from '@angular/router';
// import { Doctorstruct } from 'src/app/modal/Doctor';
// import { FirebasedoctsService } from 'src/app/services/firebasedocts.service';

// @Component({
//   selector: 'app-homeview',
//   templateUrl: './homeview.page.html',
//   styleUrls: ['./homeview.page.scss'],
// })
// export class HomeviewPage implements OnInit {

//   doctor : Doctorstruct ={
//     name:'',
//     description:'',
//     department:'',
//     phone: '',
//     specialization: '',
//     consultationFee : '',
//     timings:'',
//     facilities:'',
//     createdAt: ''
//   }

//   constructor(private activatedRoute : ActivatedRoute , private fbservice : FirebasedoctsService) { }

//   ngOnInit() {
//     const id= this.activatedRoute.snapshot.paramMap.get('id')
//     if(id){
//       this.fbservice.getDoctor(id).subscribe(docData =>{
//         this.doctor = docData;
//       });
//     }

//   }

// }

import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Doctorstruct } from 'src/app/modal/Doctor';
import { BookingService } from "src/app/services/bookingservice/booking.service";
import { DoctorService } from "src/app/services/firebasedoctor/doctor.service";
import { ActivatedRoute, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
    selector: 'app-homeview',
    templateUrl: './homeview.page.html',
    styleUrls: ['./homeview.page.scss'],
  })
export class HomeviewPage implements OnInit {

  doctor : Doctorstruct ={
    name:'',
    description:'',
    department:'',
    phone: '',
    specialization: '',
    consultationFee : '',
    timings:'',
    facilities:'',
    createdAt: ''
  } 
 

  constructor(private Aauth : AngularFireAuth,private fbservicedoctor : DoctorService , private fbservicebooking : BookingService, private activatedRoute : ActivatedRoute , private router : Router , private navctrl : NavController) { }

  ngOnInit() {
    const id= this.activatedRoute.snapshot.paramMap.get('id');
    if(id)
    {
      this.fbservicedoctor.getDoctor(id).subscribe(docData => {
        this.doctor = docData;
      });
    }
  }
 
  goback()
  {
    this.navctrl.navigateRoot("/sidemenu/sidehome/tabs/home")
  }


}
