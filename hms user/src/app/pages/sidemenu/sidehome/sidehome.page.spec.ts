import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SidehomePage } from './sidehome.page';

describe('SidehomePage', () => {
  let component: SidehomePage;
  let fixture: ComponentFixture<SidehomePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidehomePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SidehomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
