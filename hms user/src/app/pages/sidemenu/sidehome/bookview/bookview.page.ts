import { Component, OnInit  } from '@angular/core';
// import { FirebasefirestoreService } from 'src/app/services/firebasefirestore.service';
import { DoctorService } from "src/app/services/firebasedoctor/doctor.service";
import {  BookingService } from "src/app/services/bookingservice/booking.service";
import { ActivatedRoute , Router} from '@angular/router';
import { Doctorstruct, bookingstruct } from 'src/app/modal/Doctor';
import { userStruct } from "src/app/modal/patient";
import { AlertController, NavController, ToastController, Platform, ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { PatientService } from 'src/app/services/patientservice/patient.service';

import { CalendarModal, CalendarModalOptions, DayConfig, CalendarResult } from "ion2-calendar";
import * as firebase from 'firebase'
// import { Calendar } from '@ionic-native/calendar';
// import { not } from '@angular/compiler/src/output/output_ast';

// this is API which is to use calender options 
// import { Hammer } from "hammerjs";
// import { CalendarComponentOptions } from 'ion2-calendar'
// import 'hammerjs/hammer'

@Component({
  selector: 'app-bookview',
  templateUrl: './bookview.page.html',
  styleUrls: ['./bookview.page.scss'],
})
export class BookviewPage implements OnInit {

  // patient : userStruct ={
  //   userFullName:"",
  //   emailId : "", //no edits
  //   userUID : "",   //no edits
  //   createdON :null,
  //   userImageUrl : "",
  // }

  doctor : Doctorstruct ={
    name:'',
    description:'',
    department:'',
    phone: '',
    specialization: '',
    consultationFee : '',
    timings:'',
    facilities:'',
    createdAt: ''
  }

  book : bookingstruct={
    doctorID : "" ,
    bookingId:"", 
    doctorName : "",
    // bookingDate :null,   //no need to state here 
    userUID : "",
    status:'',
    userPatientListDocId : "",
    userFullName:''
  }   
 
  days:string[]=["sun","mon","tue","wed","thu","fri","sat"]
  // date: CalendarResult;
  // type: 'string';

  startDate: Date;
  endDate: Date;
  calResult :any;  //calendar result stores entire data on clicking done on the calendar
  calDecider:boolean=false;
  dateObj:Date;
  

  
patientsdb:Observable<userStruct[]>
bookingsdb: Observable<bookingstruct[]>;
  
  date: string;
  type: 'string';
  dateMulti: string[];
  dateRange: { from: string; to: string; };
  day: number;


  constructor(private modalCtrl :ModalController, private plt:Platform,private toastCtrl : ToastController,private Aauth : AngularFireAuth,private alertCtrl : AlertController ,private fbservicedoctor : DoctorService ,private fbservicepatient : PatientService, private fbservicebooking : BookingService , private router : Router,private activatedRoute : ActivatedRoute,private navCtrl:NavController) 
  {
    this.plt.ready().then(() => {
      // this.calendar.listCalendars().then(data=>{
      //  this.calendar = data; 
      // });
      this.calResult=null
      this.calDecider=false 

      this.startDate = new Date()
      this.startDate.setDate(this.startDate.getDate());
  
      this.endDate  = new Date()
      this.endDate.setDate(this.endDate.getDate() +6);
    
    });
  }

 
      async showCalendar()
      {
        await this.openCalendar()
      }


 
      async ngOnInit() {

        const id= this.activatedRoute.snapshot.paramMap.get('id')    
        if( id)
        {
         this.fbservicedoctor.getDoctor(id).subscribe(docData => {
            this.doctor=docData;
          });
        }

        let userUId = this.Aauth.auth.currentUser.uid;
        this.patientsdb = this.fbservicepatient.getPatients(userUId)
        console.log(userUId)
        // this returns all Bookings the particular user 
        // this.bookingsdb = this.fbservicebooking.getBookings(userUId);

        this.bookingsdb = this.fbservicebooking.getAllBookingsInParticular(userUId,id);




  } 
 
  async addBookingShow(patient : userStruct ,slot , slotIndex)
  { 
    console.log(this.doctor.timeSlots[this.day].slots[slotIndex].slotDateArray)  // to just test how to access doctors specific timeslot's slot 
    // console.log(`slotId is ${slot.slotId} and slotTime=${slot.slotTime}`)
    this.alertCtrl.create({
      message : "do u wanna book an appointment",
      buttons : [
        {
          text : 'cancel',
          handler:()=> {
            // console.log("")
          }
        },
        {
          text : 'ok',
          handler : async () => {
            await this.addBooking(patient,slot,slotIndex)
            // console.log("booking added")
                  // here we commented this bcz the above console log message is displayed
                  //  when the method is called  " DOESNT MATTER ITS RESULT " , IF it is successful or not
                  //  the result shoud be displayed in the method itself if successfull using "then()" promise 
                  // this just says that this method is called 
                  // -----------------DONE--------------but remember its crucial
                  
          }
        }]
    }).then((alert) =>{  
      alert.present();
    })
  }
 
async addBooking(patientInfo : userStruct,slot ,slotIndex)
{
  // below is second condition to verify ther is no booking on the same date
  // if there is no condition in html page , then this if conditioin would be useful
  
  // ################
  if(!slot.slotDateArray.includes(this.dateObj.toLocaleDateString()))
    {
                      if(patientInfo.lastBookingDate)
                      {
                        // console.log("last booking date is"+patientInfo.lastBookingDate)
                        //  as the data type of lastBookingDate is Date by default , no need to convert to "Date" to data type of Date
                        // if it is of type "any" it needs to be converted to "Date" format , so we append ".toDate()"
                        let x:string=this.getDate(patientInfo.lastBookingDate)
                        console.log("last booking date is"+x)
                        let y:string =new Date().toLocaleDateString()
                        console.log("today's booking date is"+y)

                        // u added it at the  begining ,make sure u add this only after finshing the BOOKING 
                        let todayDate =this.dateObj.toLocaleDateString() // we didnt use geyt Date of function because for just created date variable it doesnt take as a time stamp and hence we use .toLocalDateString()
                        await this.doctor.timeSlots[this.day].slots[slotIndex].slotDateArray.push(todayDate)
                    // ***************
                     // so now after listening to the above cmnt , though we pushed the data here, we didnt upload here , we uploaded at the last after confirming the booking
                        // -and By the way we added this here because , the html data viewed must be changed after a single person selecting a slot
                        //  and should not be available for another user to select the same slot at the same time.
                        // this.fbservicedoctor.updateDoctor(this.doctor)
                    // ***************             

                        if(x==y && patientInfo.limit>=0)
                        {
                          patientInfo.limit = patientInfo.limit + 1;
                          await this.finallyAdd(patientInfo ,slot ,slotIndex)
                        }
                        else if(x!=y)
                        {
                          patientInfo.lastBookingDate = new Date()
                          patientInfo.limit=1
                          await this.finallyAdd(patientInfo ,slot ,slotIndex)
                        }
                        else
                        {
                          this.alertCtrl.create({
                            message : " sry u have reached maximum limits for today to book an appointment",
                            buttons : ["ok"]      
                          }).then((alert)=>{ alert.present() })
                        }
                      }

                      else
                      {
                        let lastBookingDate = new Date()
                        let limit=1 // counts as first booking for that particular or first ever booking of the user
                        await this.fbservicepatient.updatePatient2(patientInfo , lastBookingDate , limit)
    //  updates theFIREBASE with particular patients data document " WITH THESE DETAILS " using id which is used further . 
                        await this.finallyAdd(patientInfo ,slot ,slotIndex)                    
                      }
                    

                
    }          //#############
    else{      //#############
      this.showAlert("Please select Some other Slot");
      console.log("booking not available on this date"); 
    }          //#############
                  
              
  }

  async finallyAdd(patientInfo : userStruct,slot: { slotId: any; slotTime: any; } ,slotIndex: any)
  {
    this.book.userFullName = patientInfo.userFullName;
    this.book.userPatientListDocId = patientInfo.id;

    // this.book.bookedSlot = slot   //we dont need the slotDateArray and all , so we dont include this........thats it
    this.book.slotId = slot.slotId,
    this.book.slotTime =slot.slotTime,
    this.book.slotDate = this.dateObj.toLocaleDateString(), //date for which the slot was booked, i.e. appointment date
    console.log("slot date is"+ this.book.slotDate);
    
    this.book.doctorID = this.doctor.id,
    this.book.doctorName = this.doctor.name,
    this.book.status = "unvisited",
    this.book.statusUpdatedBy="",
    this.book.reason="",
    this.book.bookingDate = new Date() , // on date when the booking for specific date is done is stored ,i.e. current date
    this.doctor.pendingBookings = this.doctor.pendingBookings + 1

    // await this.fbservicebooking.bookDoctor(this.book , patientInfo).then(async ()=>{
    //     await this.fbservicedoctor.updateDoctor(this.doctor)
    //     await this.fbservicepatient.updatePatient1(patientInfo);
    // })
 
    
    // await this.fbservicedoctor.updateDoctor(this.doctor,this.book,patientInfo)
    await this.fbservicedoctor.updateDoctor(this.doctor)
    .then(async ()=>{
      this.fbservicebooking.bookDoctor(this.book , patientInfo)
      this.fbservicepatient.updatePatient1(patientInfo);
  })

  // the above of set of operatins is divided to perform one after the other using below code of line unsing nested then's
  // await this.fbservicebooking.bookDoctor(this.book ,patientInfo,this.doctor)
  
  }

  async openCalendar() 
  {
    // Code goes here
    const options: CalendarModalOptions = {
      // pickMode : 'basic',
      title: 'select a day',
      color:'danger',       
      to:this.endDate,
      from:this.startDate,
      disableWeeks: []
    };
    let Calendar =await this.modalCtrl.create({
      component : CalendarModal,
      componentProps : { options}
    })

    Calendar.present() 

    Calendar.onWillDismiss()
    .then((data) => {
      console.log(data);  // this is entire selected date's data and role info
      if(data.role=="done")
      {
        // console.log("entire data is"+data);  // displays data and role 
        this.calResult=data.data  //calender data on clicking of done
        this.calDecider=true //boolean becomes true which helps in displaying the slots data on clicking "done" on Calendar.
       
        // console.log(data.role);   // displays role i.e. "done" bcz here if condition is role="done"
        console.log("the day in  dateObj is "+this.calResult.dateObj.getDay()); //displays day id i.e. b/w 0-6

        // let months=this.calResult.months
        // console.log("months is "+months)

        this.dateObj=this.calResult.dateObj
        console.log((this.dateObj).toLocaleDateString())
        this.day=this.dateObj.getDay()
        console.log("selected day is "+this.days[this.day]);
      }
      else
      {
        this.calResult=null
        this.calDecider=false
        this.day=null
        console.log(data.role);       
      }
    })   

  }

  getDate(timestamp : firebase.firestore.Timestamp){
    let date = timestamp.toDate()
    return date.toLocaleDateString()
  }


  showAlert(message : string)
  {
    this.alertCtrl.create({
      header: "Slot ALREADY Booked",
      message,
      buttons:["OK"]
    }).then((alert) => {
      alert.present()
    })
  }
  goback(){
    this.navCtrl.back()
  }

}

