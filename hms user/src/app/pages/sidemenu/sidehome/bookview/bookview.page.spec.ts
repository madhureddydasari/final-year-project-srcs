import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BookviewPage } from './bookview.page';

describe('BookviewPage', () => {
  let component: BookviewPage;
  let fixture: ComponentFixture<BookviewPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookviewPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BookviewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
