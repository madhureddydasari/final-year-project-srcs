import { Component, OnInit } from '@angular/core';
import { bookingstruct } from 'src/app/modal/Doctor';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
// import { FirebasefirestoreService } from 'src/app/services/firebasefirestore.service';
import {  DoctorService } from "src/app/services/firebasedoctor/doctor.service";
import { BookingService } from "src/app/services/bookingservice/booking.service";
import { NavController } from '@ionic/angular';
import { AutoSlotUpdateService } from 'src/app/services/autoslotupdate/auto-slot-update.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.page.html',
  styleUrls: ['./history.page.scss'],
})
export class HistoryPage implements OnInit {
  book : bookingstruct={
    doctorID : "" ,
    bookingId:"",
    doctorName : "",
    // bookingDate : null,
    userUID : "",
    status:'',
  } 

  bookingsdb: Observable<bookingstruct[]>;

  userId :string ;

  constructor(private fbAutoSlotUpdateService : AutoSlotUpdateService,private navCtrl : NavController,private Aauth : AngularFireAuth , private fbservicedoctor : DoctorService , private fbservicebooking : BookingService ) 
  {
    this.userId = this.Aauth.auth.currentUser.uid
    this.bookingsdb = this.fbservicebooking.getBookings(this.userId);
    console.log(this.bookingsdb);
    
    this.fbAutoSlotUpdateService.updation(this.bookingsdb);
  }  

  ngOnInit() {
    console.log(this.userId)
    this.userId = this.Aauth.auth.currentUser.uid
    this.bookingsdb = this.fbservicebooking.getBookings(this.userId);
  }

  goback(){ 
    this.navCtrl.navigateRoot('/sidemenu/sidehome')
  }

  getDate(x: { toDate: () => { (): any; new(): any; toLocaleDateString: { (): any; new(): any; }; }; })
  {
    return x.toDate().toLocaleDateString()
  }

  openSingleHistory(id1 :string,id2:string){
    this.navCtrl.navigateForward(`history/open-single-history/${id1}/${id2}`)
  }


  // getDate(timestamp : firebase.firestore.Timestamp){
  //   let date =timestamp.toDate();
  //   return date.toLocaleDateString();
  // }

}
