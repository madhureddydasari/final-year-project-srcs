import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { bookingstruct } from 'src/app/modal/Doctor';
import { BookingService } from 'src/app/services/bookingservice/booking.service';
import { NavController, ToastController, AlertController } from '@ionic/angular';
import { DoctorService } from 'src/app/services/firebasedoctor/doctor.service';
import { Doctorstruct } from "src/app/modal/Doctor";
import { Observable } from 'rxjs';
@Component({
  selector: 'app-open-single-history',
  templateUrl: './open-single-history.page.html',
  styleUrls: ['./open-single-history.page.scss'],
})
export class OpenSingleHistoryPage implements OnInit {

  bid:string;
  did:string;
  bookingsdb : Observable<bookingstruct[]>
  booking : bookingstruct={
    doctorID : "" ,
    bookingId:"",
    bookingDate:null,
    doctorName : "",
    userUID : "",
    status:'',
  } 

  doctor: Doctorstruct={
    // dateRange?: number,
    // id?: any,
    name: "",
    department:'',
    phone : '',
    description : '',
    specialization: '',
    consultationFee : '',
    timings:'',
    facilities: '',
    createdAt :'',
    // timeSlots?: any[],
  }
 
  constructor(private alertCtrl : AlertController,private toastCtrl : ToastController,private fbServiceDoctor:DoctorService,private navCtrl : NavController,private fbBookingService:BookingService,private activatedRoute :ActivatedRoute) 
  { 
    this.showToast()
  }

  // getDate(x)
  // {
  //   return x.toDate().toLocaleDateString()
  // }
  goBack(){
    this.navCtrl.back()
  }

  async cancelBooking(book:bookingstruct)
  {
    this.alertCtrl.create({
      message:"are u sure to cancel booking",
      buttons:[{text:"yes",handler:()=> this.doCancelBooking(book)},{text:"no"}]
    }).then((alert) => { alert.present() })
  }

  async doCancelBooking(book:bookingstruct){
    document.getElementById('hidden-div').style.display = 'block'; 
    document.getElementById('hidden-div').style.display = 'none'
    console.log(book.doctorID)  // working fine
    console.log(this.doctor.id)    // not working //now its working fine

    let sId=book.slotId;
    let dayOfTimeSlots = Math.floor(sId/1000)-1;
    let slotOfTimeSlots = sId%1000-1;
    let sDate=book.slotDate
    let array=await this.doctor.timeSlots[dayOfTimeSlots].slots[slotOfTimeSlots].slotDateArray
    let index = array.indexOf(sDate)
    // delete array[index] 
    // await array.pop()
    await this.doctor.timeSlots[dayOfTimeSlots].slots[slotOfTimeSlots].slotDateArray.splice(index, 1);
    // this.fbServiceDoctor.updateDoctor(this.doctor)
    this.doctor.pendingBookings-=1
    // await this.fbBookingService.cancelBooking(book,this.doctor);//
    this.fbServiceDoctor.updateDoctorById(book,this.doctor)
    this.fbBookingService.cancelBooking(book);

  }
  // getSnapshotBookingById

  async ngOnInit() {
    this.bid=this.activatedRoute.snapshot.paramMap.get('id1')
    // this.fbBookingService.getbooking(this.bid).subscribe((docdata) => {
    //   this.booking=docdata
    // }) 

    this.bookingsdb =this.fbBookingService.getSnapshotBookingById(this.bid)

    this.did=this.activatedRoute.snapshot.paramMap.get('id2')
       this.fbServiceDoctor.getDoctor(this.did).subscribe((docData)=>{
       this.doctor=docData
    })
  }

  async showToast()
  {
    await this.toastCtrl.create({
      message:"loading .......",
      duration:4000,
      position: 'middle',
    }).then((toast) => { toast.present() })
  }
  
}
