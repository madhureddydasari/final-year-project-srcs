import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OpenSingleHistoryPage } from './open-single-history.page';

const routes: Routes = [
  {
    path: '',
    component: OpenSingleHistoryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OpenSingleHistoryPageRoutingModule {}
