import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OpenSingleHistoryPageRoutingModule } from './open-single-history-routing.module';

import { OpenSingleHistoryPage } from './open-single-history.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OpenSingleHistoryPageRoutingModule
  ],
  declarations: [OpenSingleHistoryPage]
})
export class OpenSingleHistoryPageModule {}
