import { Component, OnInit } from '@angular/core';
import { RouterEvent, Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { NavController, AlertController } from '@ionic/angular';
import { userStruct } from 'src/app/modal/patient';
import { PatientService } from 'src/app/services/patientservice/patient.service';
import { Observable } from 'rxjs';



@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.page.html',
  styleUrls: ['./sidemenu.page.scss'],
})
export class SidemenuPage implements OnInit {
  
  pages =[
    {
      title:"Sidehome",
      url :"/sidemenu/sidehome"
    }, 
    {
      title:"History",
      url :"/history"
    },
    {
      title:"About",
      url :"/about"
    },
    {
      title:"Help",
      url :"/help"
    },
    
  ];

  selectedPath="";

  // patientsdb : Observable<userStruct[]>


  constructor(private fbservicepatient : PatientService,private alertCtrl : AlertController,private router : Router ,private navCtrl : NavController, private Aauth : AngularFireAuth)
  {
          this.router.events.subscribe((event: RouterEvent)=>{
            if(event && event.url){
              this.selectedPath = event.url;
            }
          })
          
        
          this.Aauth.auth.onAuthStateChanged((user)=> {
            if(user)
            {
              // this.patientsdb = this.fbservicepatient.getPatients(user.uid);
            }
            else
            {
              this.navCtrl.navigateRoot('/index/login')
            }
          })        
  }
// sss : string ;
  ngOnInit() {

//  Here it is undeined where it is a string value in abovee method or function
    // console.log("this is out of thT"+this.sss)

  }



  logout(){

    this.alertCtrl.create({
      message : "are you sure , yo want to logout",
      buttons : 
      [{
          text : "cancel "
        },
        {
         text : "Ok",
         role:"ok",
         handler : () => {
                this.Aauth.auth.signOut()
                .then(() => {
                  this.navCtrl.navigateRoot("/index/login");
                  // this.router.navigateByUrl('/', { skipLocationChange: false });
            
                  // Here navigateRoot means it allows to navigate the user to destination i.e. here it is login page
                  // But it does not allow the user to come back to the previous page ,
                  // this navigateRoot is useful to implement features like logout 
                  // once we logout of the system it signout of firebase Authentication SDK\
                  //  and also never allow the user to go back  to the previous authenticated page
                  // so the user needs to login again 
                }).catch((err) => {
                  console.log(err)
                })
         } 
      }] 
    }).then((alert) => {
      alert.present()
    })
      
  }

  viewprofile(){
    let uid=this.Aauth.auth.currentUser.uid
    this.navCtrl.navigateForward(`/viewprofile/${uid}`)
  }


} 