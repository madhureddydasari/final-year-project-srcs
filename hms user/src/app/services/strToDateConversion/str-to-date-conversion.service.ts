import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StrToDateConversionService {

  constructor() { }

  localeDateStringToDateConversion(x: string)
  {
    let dateData=x.split('/')
    let timeInMilliseconds = dateData[1] + dateData[0] + dateData[2]  
    console.log(timeInMilliseconds)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
  }

  // here covert localeDateString format from 'dd/mm/yyyy' to 'yyyy/mm/dd'
  localeDateStringToConvertibleDateString(x: string)
  {
    let dateData=x.split('/')
    let timeInMilliseconds = dateData[2] +'/'+ dateData[1] +'/'+ dateData[0]  
    console.log(timeInMilliseconds)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
    return timeInMilliseconds
  }

    // here covert localeDateString to  DATE of format from 'dd/mm/yyyy' to 'yyyy/mm/dd'
  localeDateStringToDate(x: string)
  {
    let dateData=x.split('/')
    let timeInMilliseconds = dateData[2] +'/'+ dateData[1] +'/'+ dateData[0]  
    let date=new Date(timeInMilliseconds)
    return date
  }

}
