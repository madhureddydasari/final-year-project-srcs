import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore , DocumentReference } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import {  map, take} from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';
import { ToastController, NavController } from '@ionic/angular';
import { BookingService } from '../bookingservice/booking.service';
import { Doctorstruct,bookingstruct } from "src/app/modal/Doctor";

 
@Injectable({
  providedIn: 'root'
})
export class DoctorService {

  private doctorsdb: Observable<Doctorstruct[]>
  private DoctorCollection: AngularFirestoreCollection<Doctorstruct>;
 


  constructor(private fbServiceBooking:BookingService,private navCtrl : NavController,private toastCtrl : ToastController, private afs : AngularFirestore , private Aauth : AngularFireAuth) 
  {


    this.DoctorCollection = this.afs.collection<Doctorstruct>('DoctorsList',ref => ref.where("toInclude","==",true).where("doctorExist","==",true));

    this.doctorsdb = this.DoctorCollection.snapshotChanges().pipe(
      map(actions =>{
        return actions.map(a=> {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    ) 
  }

  // to get all the doctors details
  getAllDoctors() : Observable<Doctorstruct[]>{
    return this.doctorsdb;
  }
  
  // to retrieve particular doctor details by doctor id
  getDoctor(id: string) : Observable<Doctorstruct>{
    return this.DoctorCollection.doc<Doctorstruct>(id).valueChanges().pipe(
      take(1),
      map(firestoreDocumentValue => {
        firestoreDocumentValue.id = id;
        return firestoreDocumentValue;
      })
    );
  }
  
  async updateDoctor(doctor : Doctorstruct)
  {
    return await this.DoctorCollection.doc(doctor.id)
    .set({timeSlots : doctor.timeSlots , pendingBookings : doctor.pendingBookings},{merge : true}) //we have pushed the slot date into the slotDateArray , so we have to update the timeslots Array 
  }

  async updateDoctorById(booking: bookingstruct ,doctor: Doctorstruct)
  {
    await this.DoctorCollection.doc(doctor.id)
      .set({ timeSlots: doctor.timeSlots , pendingBookings : doctor.pendingBookings }, { merge: true });
    // this.fbServiceBooking.cancelBooking(booking);
  }

  
}



