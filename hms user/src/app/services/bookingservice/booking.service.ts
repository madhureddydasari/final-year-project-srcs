import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore , DocumentReference } from '@angular/fire/firestore';
import { bookingstruct, Doctorstruct } from 'src/app/modal/Doctor';
import { userStruct } from "src/app/modal/patient";
import { Observable } from 'rxjs';
import {  map, take} from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';
import { ToastController, NavController, AlertController } from '@ionic/angular';
import { PatientService } from '../patientservice/patient.service';
import { BookviewPage } from 'src/app/pages/sidemenu/sidehome/bookview/bookview.page';
import { promise } from 'protractor';


@Injectable({
  providedIn: 'root'
})

export class BookingService {
  
  bookingsdb : Observable<bookingstruct[]>
  patientsdb : Observable<userStruct[]>

  bookingCollection : AngularFirestoreCollection<bookingstruct>
  patientsCollection : AngularFirestoreCollection<userStruct>


  constructor(private alertCtrl : AlertController,private fbservicepatient : PatientService,private navCtrl : NavController,private toastCtrl : ToastController, private afs : AngularFirestore , private Aauth : AngularFireAuth) 
  { }

//  this is to get details of a single doctor , similarly do for single particular booking
          // getDoctor(id: string) : Observable<Doctorstruct>{
          //   return this.DoctorCollection.doc<Doctorstruct>(id).valueChanges().pipe(
          //     take(1),
          //     map(firestoreDocumentValue => {
          //       firestoreDocumentValue.id = id;
          //       return firestoreDocumentValue;
          //     })
          //   );
          // }

// it shows up the list of all bookings of the current user and can be used to show in history page
          //In history page to load all bookings of current user 
  getBookings(currentuseruid: string) : Observable<bookingstruct[]>
  {
            // define data base in collection variable i.e. stores collection name for suppose
                // to find current user and get details ie. bookings of only that user based on userId of login user122222113333333333 ref => ref.where("toInclude","==",true) && ref.where("doctorExist","==",true)
                // and the userUID (i.e. uid of signedinuser )that is stored while creating
        this.bookingCollection = this.afs.collection<bookingstruct>('BookingsList' , ref => ref.where('userUID','==',currentuseruid).orderBy('bookingDate','desc')) ;
// this is to be prsent in constructor but we place here because it does load before the id retrieved from firebase
        this.bookingsdb = this.bookingCollection
        .snapshotChanges().pipe(
          map(actions =>{
            return actions.map(a=> {
              const data = a.payload.doc.data();
              const id = a.payload.doc.id;
              return { id, ...data };
            });
          })
        ) 
        return this.bookingsdb;
  }

  // in bookview to load only current user bookings with the current doctor in "DESCENDING" order
  getAllBookingsInParticular(currentuseruid: string,selectedDoctorId: string) : Observable<bookingstruct[]>
  {
            // define data base in collection variable i.e. stores collection name for suppose
                // to find current user and get details ie. bookings of only that user based on userId of login user122222113333333333 ref => ref.where("toInclude","==",true) && ref.where("doctorExist","==",true)
                // and the userUID (i.e. uid of signedinuser )that is stored while creating
        this.bookingCollection = this.afs.collection<bookingstruct>('BookingsList' , ref => ref.where('userUID','==',currentuseruid).where('doctorID','==',selectedDoctorId).orderBy('bookingDate','desc')) ;
// this is to be prsent in constructor but we place here because it does load before the id retrieved from firebase
        this.bookingsdb = this.bookingCollection
        .snapshotChanges().pipe(
          map(actions =>{
            return actions.map(a=> {
              const data = a.payload.doc.data();
              const id = a.payload.doc.id;
              return { id, ...data };
            });
          })
        ) 
        return this.bookingsdb;
  }
  
// to get details of a particular booking
  getbooking(id: string) : Observable<bookingstruct>{
    return this.bookingCollection.doc<bookingstruct>(id).valueChanges().pipe(
      take(1),
      map(firestoreDocumentValue => {
        firestoreDocumentValue.id = id;
        return firestoreDocumentValue;
      })
    );
  }

  getSnapshotBookingById(bid: string) : Observable<bookingstruct[]>
  {
            // define data base in collection variable i.e. stores collection name for suppose
                // to find current user and get details ie. bookings of only that user based on userId of login user122222113333333333 ref => ref.where("toInclude","==",true) && ref.where("doctorExist","==",true)
                // and the userUID (i.e. uid of signedinuser )that is stored while creating
        this.bookingCollection = this.afs.collection<bookingstruct>('BookingsList' , ref => ref.where('bookingId','==',bid)) ;
// this is to be prsent in constructor but we place here because it does load before the id retrieved from firebase
        this.bookingsdb = this.bookingCollection
        .snapshotChanges().pipe(
          map(actions =>{
            return actions.map(a=> {
              const data = a.payload.doc.data();
              const id = a.payload.doc.id;
              return { id, ...data };
            });
          })
        ) 
        return this.bookingsdb;
  }


// to book a doctor that means it stores details of a booking we send in book as a document on the firebase cloud firestores 
  async bookDoctor(book : bookingstruct , patientInfo : userStruct) 
  {
    try{
            let uid = this.Aauth.auth.currentUser.uid ;
            book.userUID = uid ;
          
            const ref = await this.bookingCollection.add(book);
              ref.set({ bookingId: ref.id }, { merge: true })
                // merege says that add field bookingId and everything else is same as before
                .then(() => {
                  this.showToast("booking id is :" + ref.id);
                });          
    }
          catch {
            // this.showToast("booking not confirmed please try Again ")
            this.showAlert("sry For Inconvenience","booking not confirmed please try Again", " refused due to some nentwork issues")
            patientInfo.limit--  //we incremented it by 1 at the begining of the function call of this function in the root method i.e. in bookview.ts , and as it is unsuccessful , we decrement it now
          } 
  }

  autoUpdateBookingStatus(bookingId:string)
  {
    this.bookingCollection =this.afs.collection<bookingstruct>('BookingsList')
    return this.bookingCollection.doc(bookingId).set({status:"expired"},{merge:true})
    
  }
  
  
  cancelBooking(booking:bookingstruct)
  {
    // if(!booking.reason)
    // {
    //   booking.reason=""
    // }
    this.bookingCollection.doc(booking.id).update({
      status:"cancelled",
      statusUpdatedBy:"user",
      reason:booking.reason,
    }).then(()=>{ this.navCtrl.back()})
  }

  // to show toast messsages 
    showToast(message :string) {
      this.toastCtrl.create({
        message ,
        duration : 2000
      }).then((toast) => {
        toast.present()
      })
    }

  // to show Alert messages
    showAlert(header:string ,subHeader : string, message:string ,)
    {
      this.alertCtrl.create({
        header,
        subHeader,
        message,
        buttons:["OK"]
      }).then((alert) => {
        alert.present()
      })

    }
}
