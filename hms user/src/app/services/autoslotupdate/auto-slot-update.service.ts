import { Injectable } from '@angular/core';
import { StrToDateConversionService } from '../strToDateConversion/str-to-date-conversion.service';
import { BookingService } from '../bookingservice/booking.service';
import { Observable } from 'rxjs';
import { bookingstruct, Doctorstruct } from 'src/app/modal/Doctor';
import { DoctorService } from '../firebasedoctor/doctor.service';

@Injectable({
  providedIn: 'root'
})
export class AutoSlotUpdateService {

  todaysDate : string = new Date().toLocaleDateString()
  doctor : Doctorstruct ={
    name:"",
    department:'',
    phone : "",
    description : "",
    specialization: "",
    consultationFee : "",
    timings:"",
    facilities: "",
    createdAt :"" ,
  }

  constructor(private fbTimeService : StrToDateConversionService , private fbBookingService : BookingService,private fbDoctorService : DoctorService) { }


  clearDoctor()
  {
    this.doctor.name='';
    this.doctor.description='';
    this.doctor.department='';
    this.doctor.phone= '';
    this.doctor.specialization= '';
    this.doctor.consultationFee = '';
    this.doctor.timings='';
    this.doctor.facilities='';
    this.doctor.createdAt= '';
   
  }
  
  async updation(bookingsdb1 : Observable<bookingstruct[]>)
  {
    bookingsdb1.subscribe(async (docdata) =>{ 
      // console.log(docdata);
      for(let i=0 ; i<docdata.length ; i++)
      {

        if(docdata[i].status == 'unvisited')
        {
      // console.log(docdata[i]);
          this.clearDoctor();
          //to get the doctor details to update pendingBookings attribute using the doctor id present in every single booking
          this.fbDoctorService.getDoctor(docdata[i].doctorID).subscribe((singleDoctorData) => {  this.doctor=singleDoctorData})
          if(this.convertToDate(docdata[i].slotDate) < this.convertToDate(this.todaysDate))
          {
            this.doctor.pendingBookings--
            this.fbBookingService.autoUpdateBookingStatus(docdata[i].id)
            this.fbDoctorService.updateDoctorById(docdata[i],this.doctor)
          }
        }
      }
     }) ;
  }

  convertToDate(x: string)
  {
    return this.fbTimeService.localeDateStringToDate(x)
  }
}
