import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import * as firebase from 'firebase';
import { AngularFireAuth } from "@angular/fire/auth";

@Component({
  selector: 'app-slides',
  templateUrl: './slides.component.html',
  styleUrls: ['./slides.component.scss'],
})
export class SlidesComponent implements OnInit {

  slideOpts ={
    initialSlide:0,
    // speed:400
  };

  constructor(private router :Router,private navctrl : NavController, private Aauth : AngularFireAuth)  { }

  ngOnInit() {
    
  }
  navigateTo(){
    
    // this is just to show that IF the user is logged in 
    // and then u need to navigate directly to the apps main page 
    // or else u navigate to the login or sign up page




    // this.Aauth.auth.onAuthStateChanged((user)=> {
    //   if(user){
    //     this.navctrl.navigateForward('/sidemenu/sidehome')
    //   }
    //   else{
    //     this.router.navigate(['/index/login']);  
    //   }
    // })

    // this.router.navigate(['/sidemenu']);
    // this.router.navigate(['/index/login']);  
    this.router.navigate(['/index/signup']);  

    }
    

}
