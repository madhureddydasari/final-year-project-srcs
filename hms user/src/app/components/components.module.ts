import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SlidesComponent } from './slides/slides.component';
import { IonicModule } from '@ionic/angular';
import { DeciderComponent } from './decider/decider.component';



@NgModule({
  declarations: [SlidesComponent,DeciderComponent],
  exports:[SlidesComponent],
  imports: [
    CommonModule,IonicModule
  ]
})
export class ComponentsModule { }
