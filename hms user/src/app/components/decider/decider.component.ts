import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-decider',
  templateUrl: './decider.component.html',
  styleUrls: ['./decider.component.scss'],
})
export class DeciderComponent implements OnInit {

  constructor( private Aauth : AngularFireAuth , private navCtrl : NavController) 
  { 
         //  this is to find if the user is logged in or not 
         this.Aauth.auth.onAuthStateChanged((user) => {
          if(user){
            console.log("user exists");
            this.navCtrl.navigateForward('/sidemenu/sidehome');
          }
          else
          {
            console.log("user does not exists");
            this.navCtrl.navigateForward('/index/welcome');
          }
          console.log("its done")
        })
  }

  ngOnInit() {}

}
