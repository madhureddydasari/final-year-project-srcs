import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DeciderComponent } from './decider.component';

describe('DeciderComponent', () => {
  let component: DeciderComponent;
  let fixture: ComponentFixture<DeciderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeciderComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DeciderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
